package yakiv.bondar.dev.geolocationtest;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class LocationService extends Service {
    private static final int MIN_LOCATION_TIME = 0;
    private static final int MIN_LOCATION_DISTANCE = 0;
    private LocationManager mLocationManager;
    private MyLocationListener mLocationListener;

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    private OnLocationChangedListener mListener;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("tag", "OnCreate");
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new MyLocationListener();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("tag", "onStartCommand");

        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Log.d("tag", "gps: " + isGPSEnabled);
        Log.d("tag", "netrork: " + isNetworkEnabled);
        Log.d("tag", "fine_loc: " + (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED));
        Log.d("tag", "coarse_loc: " + (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED));

        if (isPermissionGranted()) {
            if (isNetworkEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        MIN_LOCATION_TIME, MIN_LOCATION_DISTANCE, mLocationListener);
            } else if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        MIN_LOCATION_TIME, MIN_LOCATION_DISTANCE, mLocationListener);
            } else {
                Log.d("tag", "Service.Both providers are disabled");
            }
        } else {
            Log.d("tag", "Service. Permission is not granted");
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onDestroy() {
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
        //remove location update if permission granted
        if (isPermissionGranted()) {
            mLocationManager.removeUpdates(mLocationListener);
            mLocationListener = null;
        }
    }

    public interface OnLocationChangedListener {
        void locationChanged(double lat, double lng);
    }
    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(final Location loc) {
            Log.i("tag", "Location changed");
            Log.d("tag", "provider: " + loc.getProvider() + " lat: " + loc.getLatitude());
            Log.d("tag", "provider: " + loc.getProvider() + "long: " + loc.getLongitude());
            if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
                mListener.locationChanged(loc.getLatitude(), loc.getLongitude());
            }
        }

        public void onProviderDisabled(String provider) {
            Log.d("tag", "Gps Disabled");
        }

        public void onProviderEnabled(String provider) {
            Log.d("tag", "Gps Enabled");
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }
    public class LocalBinder extends Binder {
        LocationService getService(OnLocationChangedListener listener) {
            // Return this instance of LocalService so clients can call public methods
            mListener = listener;
            return LocationService.this;
        }
    }

    private boolean isPermissionGranted() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}

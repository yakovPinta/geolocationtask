package yakiv.bondar.dev.geolocationtest;

import android.Manifest;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements LocationService.OnLocationChangedListener {

    private TextView mTvLatLong;
    private Button mStart;
    private Button mStop;

    LocationService mService;
    boolean mBound = false;

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            Log.d("tag", "service class: " + service.getClass().getSimpleName());
            LocationService.LocalBinder binder = (LocationService.LocalBinder) service;
            mService = binder.getService(MainActivity.this);
            mBound = true;
            Log.d("tag", "Service binded");
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initListeners();
    }

    private void initViews() {
        mTvLatLong = (TextView) findViewById(R.id.tv_coordinates);
        mStart = (Button) findViewById(R.id.btn_start);
        mStop = (Button) findViewById(R.id.btn_stop);
    }

    private void initListeners() {
        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (isPermissionDisabled()) {
                        requestPermissions();
                    } else {
                        Log.d("tag", "StartService");
                        startAndBindLocationService();
                        mStart.setClickable(false);
                    }
                } else {
                    startAndBindLocationService();
                    mStart.setClickable(false);
                }
            }
        });

        mStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("tag", "Stop button clicked");
                mBound = false;
                stopService(new Intent(MainActivity.this, LocationService.class));
                unbindService(mConnection);
                mStart.setClickable(true);
                Log.d("tag", "isMyServiceRunning: " + isMyServiceRunning(LocationService.class));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startAndBindLocationService();
            mStart.setClickable(false);
        } else {
            Toast.makeText(this, "Please grant permission to access device location", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void locationChanged(double lat, double lng) {
        mTvLatLong.append(lat + ", " + lng + "\n");
    }

    /**
     * Check if permission enabled or not.
     * @return
     */
    private boolean isPermissionDisabled() {
        return ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check if service is running or not.
     * @param serviceClass
     * @return
     */
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Request permissions ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION
     */
    private void requestPermissions() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                1);
    }

    /**
     * Start and bind LocationService.
     */
    private void startAndBindLocationService() {
        Intent intent = new Intent(MainActivity.this, LocationService.class);
        startService(new Intent(MainActivity.this, LocationService.class));
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mBound = true;
    }
}
